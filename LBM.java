package zr;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LBM extends JPanel{
	
	enum Display {
		   VELOCITY_X, VELOCITY_Y, DENSITY, SPEED
		}
	
	private static final long serialVersionUID = 1L;
	BufferedImage bi;
    JFrame frame = new JFrame("LBM");
    
    //values to change in order to experiment:
    double vx_init = 0.05;                                        //needs to be less than c (1/sqrt(3))
    double vy_init = 0.04;
    int columns = 600; int rows = 200; int particle_num_x = columns/10; int particle_num_y = rows/10;
    double omega = 1;
    boolean switch_x = false; boolean switch_y = false;            //if true, switches orientation of init vx and/or vy (flowing in from left edge)
    boolean horizontal_borders_y = false;                          //if true, adds init vx and vy to horizontal borders(fluid streaming from there), else only vx component used (in that case, these borders are a sort of pipeline / guide)
    boolean display_particles=true;
    Display display = Display.SPEED;                              //what to display on screen
    
    int obstacle_color = Color.black.getRGB(); 
    int particle_color = Color.white.getRGB(); 
    double c = 1.0/Math.sqrt(3);
    
    double vx, vy, vx_squared, vy_squared, density_total;
    
    double[][] densityNORTH = new double[columns][rows];
    double[][] densitySOUTH = new double[columns][rows];
    double[][] densityEAST = new double[columns][rows];
    double[][] densityWEST = new double[columns][rows];
    double[][] densityNORTHEAST = new double[columns][rows];
    double[][] densityNORTHWEST = new double[columns][rows];
    double[][] densitySOUTHEAST = new double[columns][rows];
    double[][] densitySOUTHWEST = new double[columns][rows];
    double[][] densityMIDDLE = new double[columns][rows];
	double[][] density = new double[columns][rows];		                    // total density

    
    //densities for initial velocities and total density 1
    double init_densityNORTH, init_densitySOUTH, init_densityWEST, init_densityEAST, init_densityNORTHEAST, init_densityNORTHWEST, init_densitySOUTHEAST, init_densitySOUTHWEST, init_densityMIDDLE;
    
    //densities for initial velocity x (y is ignored) and density 1
    double init_density_x_NORTH, init_density_x_SOUTH, init_density_x_WEST, init_density_x_EAST, init_density_x_NORTHEAST, init_density_x_NORTHWEST, init_density_x_SOUTHEAST, init_density_x_SOUTHWEST, init_density_x_MIDDLE;
    
	double[][] x_comp = new double[columns][rows];		                     // x component of velocity
	double[][] y_comp = new double[columns][rows];                           // y component of velocity
	boolean[][] obstacles = new boolean[columns][rows]; 					//true for pixels which represent obstacles, not fluid
	
	
	double[][] particle_x = new double[particle_num_x][particle_num_y];     //contains x position particles
	double[][] particle_y = new double[particle_num_x][particle_num_y];     //contains y position particles


	public LBM()
    {
        bi = new BufferedImage(columns, rows, BufferedImage.TYPE_INT_RGB);
        ImageIcon icon = new ImageIcon( bi );
        add( new JLabel(icon) );
        
        calculate_init_densities();
        calculate_init_densities_for_x();
        
        /*XXX
          We can initialize dynamic fluid with initial velocity values, or we can initialize a static fluid  
         */
        initFluid();
        //initFluidZero();
        
        /*XXX
         * choose obstacle(s)
         */
        //make_obstacle_bowl();
		//make_obstacle_tunnel_upper();
		make_obstacle_tunnel();
        //make_obstacle_arrow_left();
        //make_obstacle_arrow_right();
        //make_obstacle_x();
        //make_obstacle_rectangles();

		//initialize particles
		for(int x = 0; x<particle_num_x; x++) {
			for (int y = 0; y < particle_num_y; y++) {
				particle_x[x][y]=10 * x;
				particle_y[x][y]=10 * y;
				
			} 
		}

    }
	
	/**
	 * calculates densities for overall density 1 and init vx and vy
	 */
	private void calculate_init_densities() {
		
		vx = vx_init;
		vy = vy_init;
		vx_squared = vx*vx;
		vy_squared = vy*vy;
		
		init_densityMIDDLE = (4.0 / 9  * (1 - 1.5 * (vx_squared + vy_squared)));
		init_densityNORTH =  (1.0 / 9 * (1 + 3 * vy + 4.5 * vy_squared - 1.5 * (vx_squared + vy_squared)));
		init_densitySOUTH =  (1.0 / 9 * (1 - 3 * vy + 4.5 * vy_squared - 1.5 * (vx_squared + vy_squared)) );
		init_densitySOUTHEAST =  (1.0 / 36  * (1 + 3 * vx - 3 * vy + 4.5 * (vx_squared + vy_squared - 2 * vx * vy) - 1.5 * (vx_squared + vy_squared)));
		init_densityNORTHEAST =  (1.0 / 36  * (1 + 3 * vx + 3 * vy + 4.5 * (vx_squared + vy_squared + 2 * vx * vy) - 1.5 * (vx_squared + vy_squared)));
		init_densityEAST = (1.0 / 9 * (1 + 3 * vx + 4.5 * vx_squared - 1.5 * (vx_squared + vy_squared)));
		init_densityWEST =  (1.0 / 9 * (1 - 3 * vx + 4.5 * vx_squared - 1.5 * (vx_squared + vy_squared)) );
		init_densityNORTHWEST = (1.0 / 36  * (1 - 3 * vx + 3 * vy + 4.5 * (vx_squared + vy_squared - 2 * vx * vy) - 1.5 * (vx_squared + vy_squared)));
		init_densitySOUTHWEST = (1.0 / 36  * (1 - 3 * vx - 3 * vy + 4.5 * (vx_squared + vy_squared + 2 * vx * vy) - 1.5 * (vx_squared + vy_squared)));
		
	}
	
	/**
	 * calculates densities for overall density 1 and init vx (ignores vy, it is zero)
	 */
	private void calculate_init_densities_for_x() {
		init_density_x_MIDDLE  = 4.0/9 * (1 - 1.5*vx_init*vx_init);
		init_density_x_NORTH  =   1.0/9 * (1 - 1.5*vx_init*vx_init);
		init_density_x_SOUTH  =   1.0/9 * (1 - 1.5*vx_init*vx_init);
		init_density_x_EAST  =   1.0/9 * (1 + 3*vx_init + 3*vx_init*vx_init);
		init_density_x_WEST =   1.0/9 * (1 - 3*vx_init + 3*vx_init*vx_init);
		init_density_x_NORTHEAST =  1.0/36 * (1 + 3*vx_init + 3*vx_init*vx_init);
		init_density_x_NORTHWEST =  1.0/36 * (1 - 3*vx_init + 3*vx_init*vx_init);
		init_density_x_SOUTHEAST =  1.0/36 * (1 + 3*vx_init + 3*vx_init*vx_init);
		init_density_x_SOUTHWEST =  1.0/36 * (1 - 3*vx_init + 3*vx_init*vx_init);
	}

	/**
	 * initialize static fluid with density 1
	 */
	private void initFluidZero() {
		
		init_horizontal_borders();
		init_vertical_borders();
		
		for (int x=1; x<columns-1; x++) {
			for (int y=1; y<rows-1; y++) {
				
				densityMIDDLE[x][y]  = 1;
				densityNORTH[x][y]  =   0;
				densitySOUTH[x][y]  =   0;
				densityEAST[x][y]  =  0;
				densityWEST[x][y]  =   0;
				densityNORTHEAST[x][y] =  0;
				densityNORTHWEST[x][y] =  0;
				densitySOUTHEAST[x][y] =  0;
				densitySOUTHWEST[x][y] = 0;
				density[x][y] = 1;
				
				x_comp[x][y] = 0;
				y_comp[x][y] = 0;
				obstacles[x][y] = false;
				
			}
		}

		
	}

	/*
	 * initializes fluid with vx_init, vy_init and density 1
	 * 
	 *  !!! breaks sooner than zero init under certain circumstances!!!
	 */
	private void initFluid() {
		vx = vx_init;
		vx_squared = vx*vx;
		vy = vy_init;
		vy_squared = vy*vy;
		
		init_horizontal_borders();
		//vertical borders have same init value as middle so we don't have to call init_vertical_borders
		
		for (int x=0; x<columns; x++) {
			for (int y=1; y<rows-1; y++) {

				density[x][y] = 1;
				
				x_comp[x][y] = vx_init;
				y_comp[x][y] = vy_init;
				obstacles[x][y] = false;
				
				densityMIDDLE[x][y] = init_densityMIDDLE;
				densityNORTH[x][y] =  init_densityNORTH;
				densitySOUTH[x][y] =  init_densitySOUTH;
				densityEAST[x][y] = init_densityEAST;
				densityWEST[x][y] =  init_densityWEST;
				densityNORTHEAST[x][y] =  init_densityNORTHEAST;
				densitySOUTHEAST[x][y] = init_densitySOUTHEAST;
				densityNORTHWEST[x][y] = init_densityNORTHWEST;
				densitySOUTHWEST[x][y] = init_densitySOUTHWEST;	
			}
		}
	}
	
	/**
	 * init left and right edge (loop edges together; constant stream coming from "between" these 2 edges defined by vx and vy)
	 */
	public void init_vertical_borders() {
		for (int y=0; y<rows; y++) {
			
			densityMIDDLE[0][y] = init_densityMIDDLE;
			densityNORTH[0][y] =  init_densityNORTH;
			densitySOUTH[0][y] =  init_densitySOUTH;
			densityEAST[0][y] = init_densityEAST;
			densityWEST[0][y] =  init_densityWEST;
			densityNORTHEAST[0][y] =  init_densityNORTHEAST;
			densitySOUTHEAST[0][y] = init_densitySOUTHEAST;
			densityNORTHWEST[0][y] = init_densityNORTHWEST;
			densitySOUTHWEST[0][y] = init_densitySOUTHWEST;	
			
			density[0][y] = 1;
			
			x_comp[0][y] = vx_init;
			y_comp[0][y] = vy_init;
			obstacles[0][y] = false;
			
			densityMIDDLE[columns-1][y] = init_densityMIDDLE;
			densityNORTH[columns-1][y] =  init_densityNORTH;
			densitySOUTH[columns-1][y] =  init_densitySOUTH;
			densityEAST[columns-1][y] = init_densityEAST;
			densityWEST[columns-1][y] =  init_densityWEST;
			densityNORTHEAST[columns-1][y] =  init_densityNORTHEAST;
			densitySOUTHEAST[columns-1][y] = init_densitySOUTHEAST;
			densityNORTHWEST[columns-1][y] = init_densityNORTHWEST;
			densitySOUTHWEST[columns-1][y] = init_densitySOUTHWEST;	
			
			density[columns-1][y] = 1;
			
			x_comp[columns-1][y] = vx_init;
			y_comp[columns-1][y] = vy_init;
			obstacles[columns-1][y] = false;
			
		}
	}


	/**
	 * upper and lower edge;
	 * first option: constant stream using vx and vy
	 * second option: constant stream using only vx
	 */
	public void init_horizontal_borders() {
		
		for (int x=0; x<columns; x++) {
				
			
			if(horizontal_borders_y) { // first option: constant stream using vx and vy
				
				densityMIDDLE[x][0]  = init_densityMIDDLE;
				densityNORTH[x][0]  =  init_densityNORTH;
				densitySOUTH[x][0]  =   init_densitySOUTH;
				densityEAST[x][0]  =   init_densityEAST;
				densityWEST[x][0]  =   init_densityWEST;
				densityNORTHEAST[x][0] =   init_densityNORTHEAST;
				densityNORTHWEST[x][0] =   init_densityNORTHWEST;
				densitySOUTHEAST[x][0] =   init_densitySOUTHEAST;
				densitySOUTHWEST[x][0] =  init_densitySOUTHWEST;
				densityMIDDLE[x][rows-1]  = init_densityMIDDLE;
				densityNORTH[x][rows-1]  =   init_densityNORTH;
				densitySOUTH[x][rows-1]  =   init_densitySOUTH;
				densityEAST[x][rows-1]  =  init_densityEAST;
				densityWEST[x][rows-1]  =   init_densityWEST;
				densityNORTHEAST[x][rows-1] =  init_densityNORTHEAST;
				densityNORTHWEST[x][rows-1] =  init_densityNORTHWEST;
				densitySOUTHEAST[x][rows-1] =   init_densitySOUTHEAST;
				densitySOUTHWEST[x][rows-1] =  init_densitySOUTHWEST;
				
				density[x][0] = 1;
				density[x][rows-1] = 1;
				
				x_comp[x][0] = vx_init;
				y_comp[x][0] = vy_init;
				obstacles[x][0] = false;
				
				x_comp[x][rows-1] = vx_init;
				y_comp[x][rows-1] = vy_init;
				obstacles[x][rows-1] = false;
				
			} else { 		// second option: constant stream using only vx

				
				densityMIDDLE[x][0]  = init_density_x_MIDDLE;
				densityNORTH[x][0]  =  init_density_x_NORTH;
				densitySOUTH[x][0]  =   init_density_x_SOUTH;
				densityEAST[x][0]  =   init_density_x_EAST;
				densityWEST[x][0]  =   init_density_x_WEST;
				densityNORTHEAST[x][0] =   init_density_x_NORTHEAST;
				densityNORTHWEST[x][0] =   init_density_x_NORTHWEST;
				densitySOUTHEAST[x][0] =   init_density_x_SOUTHEAST;
				densitySOUTHWEST[x][0] =  init_density_x_SOUTHWEST;
				densityMIDDLE[x][rows-1]  = init_density_x_MIDDLE;
				densityNORTH[x][rows-1]  =   init_density_x_NORTH;
				densitySOUTH[x][rows-1]  =   init_density_x_SOUTH;
				densityEAST[x][rows-1]  =  init_density_x_EAST;
				densityWEST[x][rows-1]  =   init_density_x_WEST;
				densityNORTHEAST[x][rows-1] =  init_density_x_NORTHEAST;
				densityNORTHWEST[x][rows-1] =  init_density_x_NORTHWEST;
				densitySOUTHEAST[x][rows-1] =   init_density_x_SOUTHEAST;
				densitySOUTHWEST[x][rows-1] =  init_density_x_SOUTHWEST;
				
				
				density[x][0] = 1;
				density[x][rows-1] = 1;
				
				x_comp[x][0] = vx_init;
				y_comp[x][0] = 0;
				obstacles[x][0] = false;
				
				x_comp[x][rows-1] = vx_init;
				y_comp[x][rows-1] = 0;
				obstacles[x][rows-1] = false;
			}
				
			
		}
	}
	
	private void make_obstacle_bowl() {
		for (int y = rows/3; y< 2*rows/3; y++) {
			obstacles[2*columns/6][y] = true;
		}
		for (int x =columns/6*3/2; x< 2*columns/6; x++ ) {
			obstacles[x][rows/3] = true; 
			obstacles[x][2*rows/3] = true; 
		}
		
	}
	
	private void make_obstacle_tunnel() {
		for(int x = 0; x< columns; x++) {
			for (int y=2; y<rows/10; y++) obstacles[x][y] = true;
			for (int y = rows-3; y>9*rows/10; y--) obstacles[x][y] = true;
		}
		
		for(int y = rows/10; y<8*rows/10; y++) {
			for(int x = columns/7; x<2*columns/7; x++) obstacles[x][y] = true;
			for(int x = 5*columns/7; x<6*columns/7; x++) obstacles[x][y] = true;
		}
		
		for(int y = rows-3; y>2*rows/10; y--) {
			for(int x = 3*columns/7; x<4*columns/7; x++) obstacles[x][y] = true;
		}
	}
	
	private void make_obstacle_tunnel_upper() {
		for(int x = 0; x< columns; x++) {
			for (int y=2; y<rows/10; y++) obstacles[x][y] = true;
			for (int y = rows-3; y>9*rows/10; y--) obstacles[x][y] = true;
		}
		
		for(int y = rows/10; y<8*rows/10; y++) {
			for(int x = columns/7; x<2*columns/7; x++) obstacles[x][y] = true;
			for(int x = 5*columns/7; x<6*columns/7; x++) obstacles[x][y] = true;
		}
		
	}
	
	private void make_obstacle_arrow_right() {
		
		for (int x =columns/6; x< columns/6+rows/4; x++) {
			obstacles[columns/3-x][x] = true; 
			obstacles[columns/3-x][rows-x] = true; 

		}		
	}
	
	private void make_obstacle_arrow_left() {
		
		for (int x =columns/6; x< columns/6+rows/4; x++) {
			obstacles[x][x] = true; 
			obstacles[x][rows-x] = true; 

		}		
	}
	
	private void make_obstacle_x() {
		
		for (int x =columns/6/2; x< columns/6+rows/4; x++) {
			obstacles[x][x] = true; 
			obstacles[x][rows-x] = true; 

		}		
		
		for (int x =columns/6/2; x< columns/6+rows/4; x += 7 ) {
			for(int i = 0; i<3; i++) {
				obstacles[x+i][x+i] = false; 
				obstacles[x+i][rows-x-i] = false;
			}
		}
	}
	
	private void make_obstacle_rectangles() {
		
		int step_x = columns/5;
		int step_y = 2*rows/7;
		
		for(int x = step_x/2; x< 3*step_x/2; x++) {
			for(int y = step_y/2; y<3*step_y/2;y++) obstacles[x][y] = true;
			for(int y = 2*step_y; y<3*step_y;y++) obstacles[x][y] = true;
		}
		
		for(int x = 2*step_x; x< 3*step_x; x++) {
			for(int y = step_y; y<5*step_y/2;y++) obstacles[x][y] = true;
		}
		
		for(int x = 7*step_x/2; x< 9*step_x/2; x++) {
			for(int y = step_y/2; y<3*step_y/2;y++) obstacles[x][y] = true;
			for(int y = 2*step_y; y<3*step_y;y++) obstacles[x][y] = true;
		}
		
	}
	
	private void collision() {
		
		//don't calculate for borders (they are constant)
		for (int x=1; x<columns-1; x++) {
			for (int y=1; y<rows-1; y++) {
				if (!obstacles[x][y]) {
					
					density_total = densityMIDDLE[x][y] + densityNORTH[x][y] + densitySOUTH[x][y] + densityEAST[x][y]
							+ densityWEST[x][y] + densityNORTHEAST[x][y] + densityNORTHWEST[x][y]
							+ densitySOUTHEAST[x][y] + densitySOUTHWEST[x][y];
					
					density[x][y] = density_total;
					
					//velocity components
					if (density_total > 0) {
						vx = (densityEAST[x][y] + densityNORTHEAST[x][y] + densitySOUTHEAST[x][y] - densityWEST[x][y]
								- densityNORTHWEST[x][y] - densitySOUTHWEST[x][y]) / density_total;
						vy = (densityNORTH[x][y] + densityNORTHEAST[x][y] + densityNORTHWEST[x][y] - densitySOUTH[x][y]
								- densitySOUTHEAST[x][y] - densitySOUTHWEST[x][y]) / density_total;
					} else {
						vx = 0;
						vy = 0;
					}
					
					x_comp[x][y] = vx;
					y_comp[x][y] = vy;
					
					vx_squared = vx * vx;
					vy_squared = vy * vy;
					
					//new density redistribution after collision
					densityMIDDLE[x][y] += omega
							* (4.0 / 9 * density_total * (1 - 1.5 * (vx_squared + vy_squared)) - densityMIDDLE[x][y]);
					densityNORTH[x][y] += omega * (1.0 / 9 * density_total
							* (1 + 3 * vy + 4.5 * vy_squared - 1.5 * (vx_squared + vy_squared)) - densityNORTH[x][y]);
					densitySOUTH[x][y] += omega * (1.0 / 9 * density_total
							* (1 - 3 * vy + 4.5 * vy_squared - 1.5 * (vx_squared + vy_squared)) - densitySOUTH[x][y]);
					densityEAST[x][y] += omega * (1.0 / 9 * density_total
							* (1 + 3 * vx + 4.5 * vx_squared - 1.5 * (vx_squared + vy_squared)) - densityEAST[x][y]);
					densityWEST[x][y] += omega * (1.0 / 9 * density_total
							* (1 - 3 * vx + 4.5 * vx_squared - 1.5 * (vx_squared + vy_squared)) - densityWEST[x][y]);
					densityNORTHEAST[x][y] += omega * (1.0 / 36 * density_total * (1 + 3 * vx + 3 * vy
							+ 4.5 * (vx_squared + vy_squared + 2 * vx * vy) - 1.5 * (vx_squared + vy_squared))
							- densityNORTHEAST[x][y]);
					densityNORTHWEST[x][y] += omega * (1.0 / 36 * density_total * (1 - 3 * vx + 3 * vy
							+ 4.5 * (vx_squared + vy_squared - 2 * vx * vy) - 1.5 * (vx_squared + vy_squared))
							- densityNORTHWEST[x][y]);
					densitySOUTHEAST[x][y] += omega * (1.0 / 36 * density_total * (1 + 3 * vx - 3 * vy
							+ 4.5 * (vx_squared + vy_squared - 2 * vx * vy) - 1.5 * (vx_squared + vy_squared))
							- densitySOUTHEAST[x][y]);
					densitySOUTHWEST[x][y] += omega * (1.0 / 36 * density_total * (1 - 3 * vx - 3 * vy
							+ 4.5 * (vx_squared + vy_squared + 2 * vx * vy) - 1.5 * (vx_squared + vy_squared))
							- densitySOUTHWEST[x][y]);
					
					
				}
				
			}
		}
		
	
			
	}

	private void stream() {
		
		for (int x=1; x<columns-1; x++) {
			for (int y=1; y<rows-1; y++) {
				densitySOUTHWEST[x][y] = densitySOUTHWEST[x+1][y+1];
				densitySOUTH[x][y] = densitySOUTH[x][y+1];
				densityWEST[x][y] = densityWEST[x+1][y];
				
			}
		}
		
		for (int x=columns-2; x>0; x--) {
			for (int y=1; y<rows-1; y++) {
				densitySOUTHEAST[x][y] = densitySOUTHEAST[x-1][y+1];
				
			}
		}
		
		for (int x=columns-2; x>0; x--) {
			for (int y=rows-2; y>0; y--) {
				densityNORTHEAST[x][y] = densityNORTHEAST[x-1][y-1];
				densityNORTH[x][y] = densityNORTH[x][y-1];
				densityEAST[x][y] = densityEAST[x-1][y];
				
			}
		}
		
		for (int x=1; x<columns-1; x++) {
			for (int y=rows-2; y>0; y--) {
				densityNORTHWEST[x][y] = densityNORTHWEST[x+1][y-1];
				
			}
		}				
		
		
		
		//stream particles
		//if it went over edge, stream into opposite side (as if in a loop)
		for(int x = 0; x<particle_num_x; x++) {
			for (int y = 0; y < particle_num_y; y++) {
				
				int i = ((int) (particle_x[x][y]));
				int j = ((int) (particle_y[x][y]));
				
				particle_x[x][y] += x_comp[i][j];
				particle_y[x][y] += y_comp[i][j];
				
				if(particle_x[x][y] > columns-1) particle_x[x][y] = 0;
				if(particle_y[x][y] > rows-1) particle_y[x][y] = 0;
				if(particle_x[x][y] < 0) particle_x[x][y] = columns-1;
				if(particle_y[x][y] < 0) particle_y[x][y] = rows-1;
			} 
		}
	
		
	}

	//bounces fluid, assuming there are no obstacles on the edges
	
	
	private void bounce() {
		for (int x=1; x<columns-1; x++) {
			for (int y=1; y<rows-1; y++) {
				if(obstacles[x][y]) {
					densityNORTH[x][y+1] += densitySOUTH[x][y];
					densitySOUTH[x][y-1] += densityNORTH[x][y];
					densityEAST[x+1][y] += densityWEST[x][y];
					densityWEST[x-1][y] += densityEAST[x][y];
					densityNORTHEAST[x+1][y+1] += densitySOUTHWEST[x][y];
					densityNORTHWEST[x-1][y+1] += densitySOUTHEAST[x][y];
					densitySOUTHEAST[x+1][y-1] += densityNORTHWEST[x][y];
					densitySOUTHWEST[x-1][y-1] += densityNORTHEAST[x][y];
					
					densitySOUTHWEST[x][y] = 0;
					densitySOUTHEAST[x][y] = 0;
					densityNORTHEAST[x][y] = 0;
					densityNORTHWEST[x][y] = 0;
					densityNORTH[x][y] = 0;
					densityEAST[x][y] = 0;
					densitySOUTH[x][y] = 0;
					densityWEST[x][y] = 0;
				}
			}
		}
	}

	private void coloring() {
		int colorValue;

		for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {

            	if(obstacles[x][y]) {
            		colorValue = obstacle_color;
            	} else {
            		            
            		float value; //this is your value between 0 and 1

            		//XXX
            		//change what number value should be multiplied with (for different init velocities and obstacles)
            		
        			switch(display) {
            		case DENSITY:
            			value = (float) (density[x][y]-0.85f)*3;
            			break;
            		case SPEED:
            			value = (float) (( Math.sqrt(Math.pow(x_comp[x][y],2) + Math.pow(y_comp[x][y],2)) ) / c) * 2.5f;
            			break;
            		case VELOCITY_X:
            			value = (float) (x_comp[x][y]/c) * 2.5f;
            			break;
            		case VELOCITY_Y:
            			value = (float) (y_comp[x][y]/c) * 2.5f;
            			break;
            		default: 
            			value = 0;
            		}
            		
            		float minHue = 0.5f; //corresponds to cyan
            		float maxHue = 1.0f; //corresponds to red
            		float hue = value*maxHue + (1-value)*minHue; 
            		colorValue = Color.HSBtoRGB(hue, 0.8f, 0.6f);
            	}

                bi.setRGB(x, y, colorValue);
                           
            }
        }
		
		if (display_particles) {
			for (int i = 0; i < particle_num_x; i++) {
				for (int j = 0; j < particle_num_y; j++) {
					int x = (int) particle_x[i][j];
					int y = (int) particle_y[i][j];
					if (!obstacles[x][y])
						bi.setRGB(x, y, particle_color);
				}
			} 
		}
	}
	

    private void createAndShowGUI()
    {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add( this );
        frame.setLocationByPlatform( true );
        frame.pack();
        frame.setVisible( true );
        
    }
    
    public void animate() {
    	
    	while(true) {
	    	
    		if(switch_x) {vx_init *= -1; calculate_init_densities_for_x(); calculate_init_densities(); init_horizontal_borders(); init_vertical_borders();}
    		if(switch_y) {vy_init *= -1; calculate_init_densities(); init_horizontal_borders(); init_vertical_borders();}
    		
    		//XXX
    		//change max j for controlling how often vx and/or vy should switch
    		
	    	for(int j = 0; j<200; j++) {
		    	for(int i=0; i<20; i++) {
	    			collision();
	    	    	stream();
	    	    	bounce();
	    	    	
	    		}
	    		coloring();
	       	 	updateUI();
	 			//try {Thread.sleep(1);} catch (InterruptedException e) {}
	    	}
    		

    	}
    	
    }

    public static void main(String[] args)
    {
    	LBM jon = new LBM();
        EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
            	
                jon.createAndShowGUI();

            }
        });
        
		jon.animate();
    }
}
